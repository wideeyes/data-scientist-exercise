# Data Science Test


## Getting Started

1. [Fork](https://bitbucket.org/wideeyes/data-scientist-exercise/fork) this repository to work on your own version.
2. Read about the tasks in the handout.pdf document.
3. You have a file `data/dataset.txt` containing the e-commerce data.
3. Commit the code to your fork of the repository then send a link to us via email.




## Good luck and happy coding!
