# encoding: utf-8
"""Example."""
import logging
import glob, os
from shutil import copy2


def main():
    """Main."""
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("Main.")
    num_files = 50000
    files = []
    for file in os.listdir("data/data/www.sammydress.com"):
        if file.endswith(".html"):
            print(os.path.join("data/data/www.sammydress.com", file))
            files.append(os.path.join("data/data/www.sammydress.com", file))
    logging.info("num files: %s", len(files))
    files = files[:num_files]
    for file in files:
        copy2(file, "data/")



if __name__ == '__main__':
    main()
